# Developer

### Update Packages

- check for updates on pypi.org and update the pyproject.toml file as needed


### Basic How To Develop

- activate virtualenv: `ve`
    - or you can source it: `source .venv/bin/activate`
- install dependencies: `uv sync`
- run app (if you run the app - e.g. `ib build ...` from the poetry shell terminal you will be using the shell (dev) instance vs the globally installed instance)


### Developer 'Testing'

Often you will want to try a build of another version to get local ib changes. You can easily do this by getting a poetry shell (see above) and then `cd` to the project you want to build and run `ib` as you need (so long as you stay in the poetry shell you will be using the local (dev) `ib` instance.


### Publishing to Pypi

- build a release: `uv build`
- publish to pypi: `uv publish dist/ibuilder-3.10.1*`
    + __NOTE:__ this assumes you set the `UV_PUBLISH_TOKEN` environment variable (_.env-setup should do this for you_)
